package main

import (
	"fmt"
	"bufio"
	"os"
	"strings"
	"strconv"
)

func main(){

	arr := make([]int, 0)

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	input := scanner.Text()

	afterSplit := strings.Split(input, " ")
	fmt.Println("input  : ",afterSplit)

	for i:=0; i<len(afterSplit); i++ {
		temp, _ :=  strconv.Atoi(afterSplit[i])
		arr  = append(arr, temp)
	}

	bubbleSort(arr)
	fmt.Println("after sort :",arr)

}

func bubbleSort(arr[] int)  {

	round := len(arr)-1
	swap := true

	for round >= 1 && swap  {

		swap = false
		for i:=0; i<round; i++  {
			if arr[i] > arr[i+1]{
				temp := arr[i+1]
				arr[i+1] = arr[i]
				arr[i] = temp
				swap = true
			}

		}
		round--
	}
	
}


